#For this problem, you aren't writing any code.
#Instead, please just change the last argument 
#in f() to maximize the output.
from math import *

def f(mu, sigma2, x):
    return 1/sqrt(2.*pi*sigma2) * exp(-.5*(x-mu)**2 / sigma2)

mu = 10
nu = 12
sigma2 = 4.0
r2 = 4.0
mup = 1.0 / (sigma2 + r2) * (sigma2 * mu + r2 * nu)
sigmap = 1.0 / ( 1.0/sigma2 + 1.0/r2 ) 
print(mup, sigmap)
print f(10.,4.,10.0) #Change the 8. to something else!

